import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'officialDash',
    templateUrl: './official-dash.component.html',
    styleUrls: ['./official-dash.component.css']
})
export class OfficialDashComponent implements OnInit {
    title = 'Busk.pro Cup stævne system';
    user: any = {
        admin: true
    };
    view = 'official';
    constructor() { }

    ngOnInit() {
    }

}
