import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OfficialRoutingModule} from "./official-routing.module";
import {OfficialDashComponent} from "./official-dash.component";

@NgModule({
  declarations: [      OfficialDashComponent
  ],
  imports: [
    CommonModule,
      OfficialRoutingModule,
  ],
    bootstrap:[OfficialDashComponent]
})
export class OfficialModule { }
