import { TestBed } from '@angular/core/testing';

import { OfficialhttpService } from './officialhttp.service';

describe('OfficialhttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OfficialhttpService = TestBed.get(OfficialhttpService);
    expect(service).toBeTruthy();
  });
});
