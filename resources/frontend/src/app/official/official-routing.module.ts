import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OfficialDashComponent} from "./official-dash.component";

const routes: Routes = [{
    path:'',component:OfficialDashComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfficialRoutingModule { }
