import { Component } from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Busk.pro Cup stævne system';
  user: any = {
    admin: true
  };
  view = 'admin';
    mode = new FormControl('over');
}
