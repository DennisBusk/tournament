import { NgModule } from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

const appRoutes: Routes = [
    {
        path: '', redirectTo: '/guest', pathMatch: 'full' },
    {
        path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
    {
        path: 'official', loadChildren: './official/official.module#OfficialModule' },
    {
        path: 'guest', loadChildren: './guest/guest.module#GuestModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes) ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
