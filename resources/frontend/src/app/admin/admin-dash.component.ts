import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'adminDash',
    templateUrl: './admin-dash.component.html',
    styleUrls: ['./admin-dash.component.css']
})
export class AdminDashComponent implements OnInit {
    title = 'Busk.pro Cup stævne system';
    user: any = {
        admin: true
    };
    view = 'admin';
    constructor() { }

    ngOnInit() {
    }

}
