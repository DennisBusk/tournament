import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminDashComponent} from "./admin-dash.component";
import {FightersComponent} from "./fighters/fighters.component";
import {FighterdetailComponent} from "./fighters/fighterdetail/fighterdetail.component";
import {EditfighterComponent} from "./fighters/editfighter/editfighter.component";

const adminRoutes: Routes = [
    {
        path: '',
        component: AdminDashComponent,
        children: [{
            path:'fighters',
            component: FightersComponent,
            children:[{
            path: 'fighter',
            component: FighterdetailComponent,
            children:[{
                path: 'edit',
                component: EditfighterComponent
            }]
        }]
        }]
    }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
