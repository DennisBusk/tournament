import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import {AddtournamentComponent} from './tournament/addtournament/addtournament.component';
import {EdittournamentComponent} from './tournament/edittournament/edittournament.component';
import {FightersComponent} from './fighters/fighters.component';
import {AddfighterComponent} from './fighters/addfighter/addfighter.component';
import {EditfighterComponent} from './fighters/editfighter/editfighter.component';
import {MatchComponent} from "./match/match.component";
import {FighterdetailComponent} from './fighters/fighterdetail/fighterdetail.component';
import {MatButtonModule, MatCardModule, MatDialogModule, MatListModule, MatGridListModule} from "@angular/material";
import {AdminDashComponent} from "./admin-dash.component";

@NgModule({
    declarations: [AdminDashComponent, AddtournamentComponent, EdittournamentComponent, MatchComponent, FightersComponent, AddfighterComponent, EditfighterComponent, FighterdetailComponent],
    imports: [
        CommonModule,
    AdminRoutingModule,
        MatButtonModule,
        MatListModule,
        MatCardModule,
        MatDialogModule,
        MatGridListModule,
        AdminRoutingModule,
    ],
    exports:[
    ],
    entryComponents: [
FighterdetailComponent
        ]
})
export class AdminModule {
}
