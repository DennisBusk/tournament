import { Component, OnInit } from '@angular/core';
import {MatDialog} from '../../../../node_modules/@angular/material';
import {FighterdetailComponent} from "./fighterdetail/fighterdetail.component";

@Component({
  selector: 'fighters',
  templateUrl: './fighters.component.html',
  styleUrls: ['./fighters.component.css']
})
export class FightersComponent implements OnInit {
fighters =   [
      {
          avatar: 'https://ya-webdesign.com/transparent600_/mma-vector-silhouette.png',
          name: 'Test Fighter 1',
          grade : '1. Dan',
          experience: '3 år',
          description : 'Lorem Ipsum er ganske enkelt fyldtekst fra print- og typografiindustrien. Lorem Ipsum har været standard fyldtekst siden 1500-tallet, hvor en ukendt trykker sammensatte en tilfældig spalte for at trykke en bog til sammenligning af forskellige skrifttyper. Lorem Ipsum har ikke alene overlevet fem århundreder, men har også vundet indpas i elektronisk typografi uden væsentlige ændringer. Sætningen blev gjordt kendt i 1960\'erne med lanceringen af Letraset-ark, som indeholdt afsnit med Lorem Ipsum, og senere med layoutprogrammer som Aldus PageMaker, som også indeholdt en udgave af Lorem Ipsum.',
          weight: 95,
          age: 21,
          height: 186,
          info:[
              'Hvor', 'Kom','Jeg','Fra'
          ]
      },
{
    avatar: 'https://ya-webdesign.com/transparent600_/mma-vector-silhouette.png',
    name: 'Test Fighter 2',
    grade : '1. Dan',
    experience: '2 år',
    description : 'Lorem Ipsum er ganske enkelt fyldtekst fra print- og typografiindustrien. Lorem Ipsum har været standard fyldtekst siden 1500-tallet, hvor en ukendt trykker sammensatte en tilfældig spalte for at trykke en bog til sammenligning af forskellige skrifttyper. Lorem Ipsum har ikke alene overlevet fem århundreder, men har også vundet indpas i elektronisk typografi uden væsentlige ændringer. Sætningen blev gjordt kendt i 1960\'erne med lanceringen af Letraset-ark, som indeholdt afsnit med Lorem Ipsum, og senere med layoutprogrammer som Aldus PageMaker, som også indeholdt en udgave af Lorem Ipsum.',
    weight: 95,
    age: 21,
    height: 186,
    info:[
        'Hvor', 'Kom','Jeg','Fra'
        ]
},
{
    avatar: 'https://ya-webdesign.com/transparent600_/mma-vector-silhouette.png',
        name: 'Test Fighter 3',
        grade : '1. Dan',
    experience: '1 år',
    description : 'Lorem Ipsum er ganske enkelt fyldtekst fra print- og typografiindustrien. Lorem Ipsum har været standard fyldtekst siden 1500-tallet, hvor en ukendt trykker sammensatte en tilfældig spalte for at trykke en bog til sammenligning af forskellige skrifttyper. Lorem Ipsum har ikke alene overlevet fem århundreder, men har også vundet indpas i elektronisk typografi uden væsentlige ændringer. Sætningen blev gjordt kendt i 1960\'erne med lanceringen af Letraset-ark, som indeholdt afsnit med Lorem Ipsum, og senere med layoutprogrammer som Aldus PageMaker, som også indeholdt en udgave af Lorem Ipsum.',
    weight: 95,
    age: 21,
    height: 186,
    info:[
    'Hvor', 'Kom','Jeg','Fra'
]
}
      ];
  constructor(public dialog: MatDialog,) { }

  ngOnInit() {
  }
    loadFighterDialog(fighter){
      console.log(fighter);
let figherDialog = this.dialog.open(FighterdetailComponent,{data: fighter});
    }
}
