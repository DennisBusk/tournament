import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-fighterdetail',
  templateUrl: './fighterdetail.component.html',
  styleUrls: ['./fighterdetail.component.css']
})
export class FighterdetailComponent implements OnInit {
fighter = {
  name: 'Test Fighter',
    avatar: 'https://ya-webdesign.com/transparent600_/mma-vector-silhouette.png',
    grade : '1. Dan',
    description : 'Lorem Ipsum er ganske enkelt fyldtekst fra print- og typografiindustrien. Lorem Ipsum har været standard fyldtekst siden 1500-tallet, hvor en ukendt trykker sammensatte en tilfældig spalte for at trykke en bog til sammenligning af forskellige skrifttyper. Lorem Ipsum har ikke alene overlevet fem århundreder, men har også vundet indpas i elektronisk typografi uden væsentlige ændringer. Sætningen blev gjordt kendt i 1960\'erne med lanceringen af Letraset-ark, som indeholdt afsnit med Lorem Ipsum, og senere med layoutprogrammer som Aldus PageMaker, som også indeholdt en udgave af Lorem Ipsum.',
    weight: 95,
    age: 21,
    height: 186,
    info:[
'Hvor', 'Kom','Jeg','Fra'
    ]
    }
  constructor(public dialogRef: MatDialogRef<FighterdetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  this.fighter = data;
  console.log(this.fighter);
  }

  ngOnInit() {
  }
    closeDialog() {
        this.dialogRef.close();
    }

}
