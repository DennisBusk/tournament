import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FighterdetailComponent } from './fighterdetail.component';

describe('FighterdetailComponent', () => {
  let component: FighterdetailComponent;
  let fixture: ComponentFixture<FighterdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FighterdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FighterdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
