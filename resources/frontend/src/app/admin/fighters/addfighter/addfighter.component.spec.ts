import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddfighterComponent } from './addfighter.component';

describe('AddfighterComponent', () => {
  let component: AddfighterComponent;
  let fixture: ComponentFixture<AddfighterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddfighterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddfighterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
