import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditfighterComponent } from './editfighter.component';

describe('EditfighterComponent', () => {
  let component: EditfighterComponent;
  let fixture: ComponentFixture<EditfighterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditfighterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditfighterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
