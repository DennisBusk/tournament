import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatMenuModule} from "@angular/material";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {AppRoutingModule} from "./app-routing.module";

@NgModule({
  imports: [
    BrowserModule,
      BrowserAnimationsModule,
      MatMenuModule,
      MatSidenavModule,
      MatButtonModule,
      MatListModule,
      ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
      AppRoutingModule
  ],
    declarations: [
        AppComponent,
        MenuComponent,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {


}
