import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GuestDashComponent} from "./guest-dash.component";

const guestRoutes: Routes = [
    {
      path:'',component:GuestDashComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(guestRoutes)],
  exports: [RouterModule]
})
export class GuestRoutingModule { }
