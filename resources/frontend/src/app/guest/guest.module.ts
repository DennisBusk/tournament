import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {GuestRoutingModule} from "./guest-routing.module";
import {GuestDashComponent} from "./guest-dash.component";

@NgModule({
  declarations: [      GuestDashComponent
  ],
  imports: [
    CommonModule,
      GuestRoutingModule,
  ],
    bootstrap:[GuestDashComponent]
})
export class GuestModule { }
