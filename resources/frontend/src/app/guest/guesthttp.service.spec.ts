import { TestBed } from '@angular/core/testing';

import { GuesthttpService } from './guesthttp.service';

describe('GuesthttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GuesthttpService = TestBed.get(GuesthttpService);
    expect(service).toBeTruthy();
  });
});
