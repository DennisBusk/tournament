import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'guestDash',
    templateUrl: './guest-dash.component.html',
    styleUrls: ['./guest-dash.component.css']
})
export class GuestDashComponent implements OnInit {
    title = 'Busk.pro Cup stævne system';
    user: any = {
        admin: true
    };
    view = 'guest';
    constructor() { }

    ngOnInit() {
    }

}
